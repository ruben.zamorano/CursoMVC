﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using BaseMVC.Models;
using BaseMVC.Linq;
using System.Web.Security;
using BaseMVC.Helpers.Funciones;
using BaseMVC.Helpers.Sesion;
using System.Collections.Generic;

namespace BaseMVC.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult LogIn()
        {
            
            return View("Login");
        }

        [AllowAnonymous]
        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            //Eliminar datos de sesion
            Session.Clear();
            Session.RemoveAll();
            Session.Abandon();
            return RedirectToAction("logIn", "Account");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIIn(AccountModel user)
        {
            if (ModelState.IsValid) //Verificar que el modelo de datos sea valido en cuanto a la definición de las propiedades
            {
                BaseMVCDataContext db = new BaseMVCDataContext();
                if (Isvalid(user.Email, user.Password, db))//Verificar que el email y clave exista utilizando el método privado 
                {

                    FormsAuthentication.SetAuthCookie(user.Email, false); //crea variable de usuario 

                
                    DataSesion CI = new DataSesion();
                    CI.nIdUsuario = 23;
                    CI.bSesionActiva = true;
                    CI.cLogin = user.Email;
                    this.Session["DataSesion"] = CI;

                    return Json(new { bError = false, url = "/Alumno/carga/" }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //ModelState.AddModelError("", "Login data in incorrect"); //adicionar mensaje de error al model 

                    user.Email = "";
                    user.Password = "";
                    return Json(new { bError = true, cError = "Constraseña o Usuario incorrectos" }, JsonRequestBehavior.AllowGet);
                    //return View(user);
                }
            }
            else
            {
                return View(user);
            }
            
        }

       
        private bool Isvalid(string cLogin, string password, BaseMVCDataContext db)
        {
            bool Isvalid = true;
            
            return Isvalid;
        }

        [AllowAnonymous]
        public ActionResult onClickCifrar()
        {
            RijndaelCrypt Criptografo = new RijndaelCrypt();
            var nuestra_cadena = "1";
            string encriptada = Criptografo.Encrypt(nuestra_cadena);
            string desencriptada = Criptografo.Decrypt(encriptada);

            return Json(new
            {
                encriptada,
                desencriptada
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Error()
        {

            return View("Error");
        }
        public ActionResult E500()
        {

            return View("Error");
        }
        public ActionResult E404()
        {

            return View("Error");
        }

      
    }
}