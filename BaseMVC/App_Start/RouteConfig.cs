﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BaseMVC
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "LogIn", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //name: "MyCustomRoute",
            //url: "CartaPorte/{action}/{id}",
            //defaults: new
            //{
            //    area = "Administracion",
            //    controller = "CartaPorte",
            //    action = "Index2",
            //    id = UrlParameter.Optional
            //});

            //routes.MapRoute(
            //    "mainmenu" /* Route name */,
            //    "Administracion/CartaPorte/Index" /* URL with parameters */,
            //    new { controller = "CartaPorte", action = "Index" /* Parameter defaults */ }
            //);

            //routes.MapRoute("mainmenu", "{controller}/{action}/{id}/{*catchall}",
            //new
            //{
            //    controller = "Home",
            //    action = "Index",
            //    id = UrlParameter.Optional
            //}); 

          

           
        }
    }
}
