
var BaseMVC = angular.module('BaseMVC', ['ngMaterial']);





BaseMVC.factory("Fun", function () {
    return {
        MensajeGenerico: function ($mdDialog, Encabezado, Mensaje) {
            $mdDialog.show(
                  $mdDialog.alert()
                    .parent(angular.element(document.querySelector('#Body_Layout')))
                    .clickOutsideToClose(true)
                    .title(Encabezado)
                    .textContent(Mensaje)
                    .ariaLabel('Alert Dialog Demo')
                    .ok('Entendido')
                );
        },
        Base64: function (encoded) {
            var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;

            do {
                enc1 = keyStr.indexOf(encoded.charAt(i++));
                enc2 = keyStr.indexOf(encoded.charAt(i++));
                enc3 = keyStr.indexOf(encoded.charAt(i++));
                enc4 = keyStr.indexOf(encoded.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            } while (i < encoded.length);

            return output;
        }
    };
})







