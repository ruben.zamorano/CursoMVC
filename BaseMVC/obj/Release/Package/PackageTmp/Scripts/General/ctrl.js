﻿
app.controller('Inventarios22Ctrl', ['$http', '$scope', '$filter', '$q', '$timeout', '$log', '$mdDialog', '$mdMedia', '$mdToast', function ($http, $scope, $filter, $q, $timeout, $log, $mdDialog, $mdMedia, $mdToast) {


$scope.showActionToast = function () {
    var pinTo = "left top right";
    var toast = $mdToast.simple()
      .parent(angular.element(document.querySelector('#warp')))
      .textContent('Usuario o contraseña incorrecta!')
      .action('Ok')
      .highlightAction(true)
      .highlightClass('md-primary')// Accent is used by default, this just demonstrates the usage.
      .position(pinTo);
    $mdToast.show(toast).then(function (response) {
        
    });
    
};

$scope.CambiarPass = function (ev) {
    var confirm = $mdDialog.prompt()
               .parent(angular.element(document.querySelector('#Body_Layout')))
              .title('Olvidaste tu contreseña?')
              .textContent('Ingresa tu Usuario o Email para enviarte un correo de reestablecimiento de contraseña')
              .placeholder('Usuario o Email')
              .ariaLabel('Usuario o Email')
              .targetEvent(ev)
              .ok('Recuperar!')
              .cancel('Cancelar');
    $mdDialog.show(confirm).then(function (result) {
        $scope.status = 'You decided to name your dog ' + result + '.';
    }, function () {
        $scope.status = 'You didn\'t name your dog.';
    });
}





$scope.Login = function () {
    debugger

    $http({
        url: '/Account/LogIIn',
        method: "POST",
        params: {
            'Email': $scope.usuario,
            'Password': $scope.pass
        },
    }).success(function (response) {
        debugger
        if (!response.bError) {
            $(".admin").addClass("up").delay(100).fadeOut(100);
            $(".cms").addClass("down").delay(150).fadeOut(100);
            window.location.href = response.url;
        }
        else {
            $scope.showActionToast();
        }

    }).error(function (error) {
        debugger
    });
}





$scope.MensajeGenerico = function (Encabezado, Texto) {
    $mdDialog.show(
          $mdDialog.alert()
            .parent(angular.element(document.querySelector('#Body_Layout')))
            .clickOutsideToClose(true)
            .title(Encabezado)
            .textContent(Texto)
            .ariaLabel('Alert Dialog Demo')
            .ok('Entendido')
        );

};










}]);
