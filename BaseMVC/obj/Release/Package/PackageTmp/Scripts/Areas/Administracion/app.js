﻿
var AdminLTE = angular.module('AdminLTE', ['ngMaterial']);


AdminLTE.config(['$mdThemingProvider', function ($mdThemingProvider) {
    var customPrimary = {
        '50': '#99c5de',
        '100': '#86bad8',
        '200': '#72afd2',
        '300': '#5fa4cc',
        '400': '#4c99c6',
        '500': '#3C8DBC',
        '600': '#367ea9',
        '700': '#307095',
        '800': '#296182',
        '900': '#23536f',
        'A100': '#acd0e5',
        'A200': '#c0dbeb',
        'A400': '#d3e6f1',
        'A700': '#1d445b'
    };
    $mdThemingProvider
        .definePalette('customPrimary',
                        customPrimary);

    var customAccent = {
        '50': '#000000',
        '100': '#000000',
        '200': '#040607',
        '300': '#0d1418',
        '400': '#162328',
        '500': '#1f3139',
        '600': '#314d59',
        '700': '#3a5b6a',
        '800': '#436a7a',
        '900': '#4c788b',
        'A100': '#314d59',
        'A200': '#283F49',
        'A400': '#1f3139',
        'A700': '#55869b'
    };
    $mdThemingProvider.definePalette('customAccent',customAccent);

    

    var customBackground = {
        '50': '#737373',
        '100': '#666666',
        '200': '#595959',
        '300': '#4d4d4d',
        '400': '#404040',
        '500': '#333',
        '600': '#262626',
        '700': '#1a1a1a',
        '800': '#0d0d0d',
        '900': '#000000',
        'A100': '#808080',
        'A200': '#8c8c8c',
        'A400': '#999999',
        'A700': '#000000'
    };
    $mdThemingProvider.definePalette('customBackground',customBackground);



    $mdThemingProvider.theme('default')
    .primaryPalette('customPrimary')
    .accentPalette('customAccent');
    //.warnPalette('customWarn')
    //.backgroundPalette('customBackground');


}]);

AdminLTE.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);







