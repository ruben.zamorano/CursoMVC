using BaseMVC.Helpers.DTO.Maestro;
using BaseMVC.Linq;
using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Maestro.Models
{
    public class MateriasModel: CargaInicial
    {
        public string cCargaInicial { get; set; }
        public MateriasModel()
        {
            cModuloActual = "Materias";
            nIdAreaActual = 14;
        }
        public string CargaInicial(JavaScriptSerializer js)
        {

            DTO_CargaInicialcatalogo CargaInicial = new DTO_CargaInicialcatalogo();
            string cCargaInicial = "";
            try
            {
                CargaInicial.cUrl = "/Maestro/Materias/";
                CargaInicial.cUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(CargaInicial.cUrl));
            }
            catch (Exception e)
            {
                CargaInicial.bError = true;
                CargaInicial.cMensaje = e.Message;
            }

            cCargaInicial = js.Serialize(CargaInicial);
            return cCargaInicial;
        }

    }
}
