using BaseMVC.Areas.Maestro.Models;
using BaseMVC.Helpers.Funciones;
using BaseMVC.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Maestro.Controllers
{
    public class catalogoController : Controller
    {
        BaseMVCDataContext db = new BaseMVCDataContext();
        JavaScriptSerializer js = new JavaScriptSerializer();

        public ActionResult Index()
        {
            catalogoModel model = new catalogoModel();
            model.cCargaInicial = model.CargaInicial(js);
            return View("Index",model);
        }
       


        [HttpPost]
        [AllowAnonymous]//Este encabezado es para que el metodo pueda ser llamado desde la vista por un ajax
        public ActionResult MetodoEjemplo(string someValue)
        {
            bool bError = false;
            string cMensaje = "holis";
            try
            {
                db.Connection.Open();
                db.Transaction = db.Connection.BeginTransaction();

                db.Transaction.Commit();

            }
            catch (Exception e)
            {
                db.Transaction.Rollback();
                bError = true;
            }

            finally
            {
                db.Transaction.Dispose();
            }
            return Json(new {
                cMensaje,
                bError
            }, JsonRequestBehavior.AllowGet);
        }

    }
}
