using BaseMVC.Helpers.DTO.Alumno;
using BaseMVC.Linq;
using BaseMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace BaseMVC.Areas.Alumno.Models
{
    public class tareasModel: CargaInicial
    {
        public string cCargaInicial { get; set; }
        public tareasModel()
        {
            cModuloActual = "tareas";
            nIdAreaActual = 13;
        }
        public string CargaInicial(JavaScriptSerializer js)
        {

            DTO_CargaInicialtareas CargaInicial = new DTO_CargaInicialtareas();
            string cCargaInicial = "";
            try
            {
                CargaInicial.cUrl = "/Alumno/tareas/";
                CargaInicial.cUrl = Convert.ToBase64String(Encoding.UTF8.GetBytes(CargaInicial.cUrl));
            }
            catch (Exception e)
            {
                CargaInicial.bError = true;
                CargaInicial.cMensaje = e.Message;
            }

            cCargaInicial = js.Serialize(CargaInicial);
            return cCargaInicial;
        }

    }
}
