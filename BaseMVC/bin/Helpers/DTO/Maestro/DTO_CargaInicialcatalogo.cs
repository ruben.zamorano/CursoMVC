using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Maestro
{
    public class DTO_CargaInicialcatalogo
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }

        public DTO_CargaInicialcatalogo()
        {
            bError = false;
        }
    }
}
