using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Alumno
{
    public class DTO_CargaInicialtareas
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }

        public DTO_CargaInicialtareas()
        {
            bError = false;
        }
    }
}
