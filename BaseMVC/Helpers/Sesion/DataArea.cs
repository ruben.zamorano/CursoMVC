﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.Sesion
{
    public class DataArea
    {
        public string cArea { get; set; }
        public int nIdArea { get; set; }
        public string cIconoArea { get; set; }
        public List<Modulo> lstModulos { get; set; }
    }

    public class Modulo
    {
        public string cController { get; set; }
        public string cModulo { get; set; }
        public string cURL { get; set; }
        
    }
}