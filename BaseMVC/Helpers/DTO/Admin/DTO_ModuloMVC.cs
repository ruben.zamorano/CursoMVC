﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Admin
{
    public class DTO_ModuloMVC
    {
        public string cNombreModulo { get; set; }
        public bool bError { get; set; }

        public DTO_ModuloMVC()
        {
            bError = false;
        }
    }
}