﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Admin
{
    public class DTO_CargaInicialAdmin
    {
        public string cUrl { get; set; }
        public bool bError { get; set; }
        public string cMensaje { get; set; }
        public List<DTO_AreaMVC> lstAreas { get; set; }
        public List<DTO_Perfil> lstPerfiles { get; set; }
        public List<DTO_Usuario> lstUsuarios { get; set; }

        public DTO_CargaInicialAdmin()
        {
            bError = false;
            lstAreas = new List<DTO_AreaMVC>();
            lstPerfiles = new List<DTO_Perfil>();
            lstUsuarios = new List<DTO_Usuario>();
        }
    }
}