﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Admin
{
    public class DTO_Perfil
    {
        public int nIdPerfil { get; set; }
        public string cPerfil { get; set; }
        public string cComentario { get; set; }

        public string cMensaje { get; set; }
        public bool bError { get; set; }
        public DTO_Perfil()
        {
            bError = false;
        }
    }
}