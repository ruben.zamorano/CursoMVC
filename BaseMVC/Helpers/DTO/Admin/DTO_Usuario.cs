﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Admin
{
    public class DTO_Usuario
    {
        public int nIdusuaio { get; set; }
        public string cNombres { get; set; }
        public string cLogin { get; set; }
        public string cComentario { get; set; } 
        public string cMensaje { get; set; }
        public bool bError { get; set; }
        public List<DTO_Perfil> lstPerfiles { get; set; }

        public DTO_Usuario()
        {
            lstPerfiles = new List<DTO_Perfil>();
        }
    }
}