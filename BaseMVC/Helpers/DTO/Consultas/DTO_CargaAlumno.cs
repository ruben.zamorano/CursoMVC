﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Consultas
{
    public class DTO_CargaAlumno
    {
        public int nIdCargaAlumno { get; set; }
        public int nIdAlumno { get; set; }
        public int nIdCargaMateria { get; set; }
    }
}