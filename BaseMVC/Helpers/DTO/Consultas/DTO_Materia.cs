﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Consultas
{
    public class DTO_Materia
    {
        public int nIdMateria { get; set; }
        public string cDescripcion { get; set; }
    }

}