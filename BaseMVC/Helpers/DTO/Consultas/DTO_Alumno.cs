﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Consultas
{
    public class DTO_Alumno
    {
        public int nIdAlumno { get; set; }
        public string cNombreAlumno { get; set; }
    }
}