﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Consultas
{
    public class DTO_Horario
    {
        public int nIdHorario { get; set; }
        public string cDescripcion { get; set; }
    }
}