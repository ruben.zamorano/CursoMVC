﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Consultas
{
    public class DTO_Maestro
    {
        public int nIdMaestro { get; set; }
        public string cNombreMaestro { get; set; }
    }
}