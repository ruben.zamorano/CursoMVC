﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BaseMVC.Helpers.DTO.Consultas
{
    public class DTO_CargaMateria
    {
        public int nIdCargaMateria { get; set; }
        public string cDescripcion { get; set; }

        public int nIdHorario { get; set; }
        public int nIdMateria { get; set; }
        public int nIdMaestro { get; set; }
    }
}